/**
 * @flow
 * @relayHash 6f58cf3ee4c26f7dcb740a5113c4842b
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type ApartmentItem_apartment$ref = any;
export type routes_ApartmentsItem_QueryVariables = {|
  slug: string,
|};
export type routes_ApartmentsItem_QueryResponse = {|
  +apartment: ?{|
    +$fragmentRefs: ApartmentItem_apartment$ref,
  |},
|};
*/


/*
query routes_ApartmentsItem_Query(
  $slug: String!
) {
  apartment(slug: $slug) {
    ...ApartmentItem_apartment
    id
  }
}

fragment ApartmentItem_apartment on Apartment {
  id
  title
  slug
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "slug",
    "type": "String!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "slug",
    "variableName": "slug",
    "type": "String"
  }
];
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "routes_ApartmentsItem_Query",
  "id": null,
  "text": "query routes_ApartmentsItem_Query(\n  $slug: String!\n) {\n  apartment(slug: $slug) {\n    ...ApartmentItem_apartment\n    id\n  }\n}\n\nfragment ApartmentItem_apartment on Apartment {\n  id\n  title\n  slug\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "routes_ApartmentsItem_Query",
    "type": "Root",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "apartment",
        "storageKey": null,
        "args": v1,
        "concreteType": "Apartment",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "ApartmentItem_apartment",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "routes_ApartmentsItem_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "apartment",
        "storageKey": null,
        "args": v1,
        "concreteType": "Apartment",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "id",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "title",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "slug",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  }
};
})();
(node/*: any*/).hash = '9bbc5b1878ce67054ef0b4b129143d05';
module.exports = node;

import makeRouteConfig from 'found/lib/makeRouteConfig';
import Route from 'found/lib/Route';
import React from 'react';
import { graphql } from 'react-relay';

import App from './App';
import ApartmentsList from './components/ApartmentsList';
import ApartmentItem from './components/ApartmentItem';
import New from './components/New';

const AppQuery = graphql`
    query routes_App_Query {
        viewer {
            ...App_viewer
        }
    }
`

const ApartmentsListQuery = graphql`
    query routes_ApartmentsList_Query {
        viewer {
            ...ApartmentsList_viewer
        }
    }
`;

const ApartmentsItemQuery = graphql`
    query routes_ApartmentsItem_Query($slug: String!) {
        apartment(slug: $slug) {
            ...ApartmentItem_apartment
        }
    }
`;

export default makeRouteConfig(
    <Route
        path="/"
        Component={App}
        query={AppQuery}
    >
        <Route
            Component={ApartmentsList}
            query={ApartmentsListQuery}
        />
        <Route
            path="new"
            Component={New}
        />
        <Route
            path="/apartment/:slug"
            Component={ApartmentItem}
            query={ApartmentsItemQuery}
            prepareVariables={params => ({ ...params})}
        />
    </Route>,
);

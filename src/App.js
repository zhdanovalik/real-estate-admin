import React, { Component } from 'react';
import {createFragmentContainer, graphql} from 'react-relay';
import { Link } from 'found';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';
import './App.css';
import 'antd/dist/antd.css';
import { Layout, Menu, Icon } from 'antd';
const { Header, Sider, Content } = Layout;

class App extends Component {
    state = {
        collapsed: false,
    }
    render() {
        return (
            <Layout>
                <Sider
                    trigger={null}
                    collapsible
                    collapsed={this.state.collapsed}
                >
                </Sider>
                <div>
                    <Link to="/">
                        home
                    </Link>
                    <Link to="/new">
                        new
                    </Link>
                    <p>
                        To get started, edit <code>src/App.js</code> and save to reload.
                    </p>
                    {this.props.children}
                </div>
            </Layout>
        );
    }
}

export default createFragmentContainer(
    App,
    graphql`
        fragment App_viewer on User {
            id,
        }
    `
);

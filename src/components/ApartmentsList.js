import React from 'react';
import { createFragmentContainer, graphql } from 'react-relay';
import { Link } from 'found';
import RemoveApartmentMutation from '../mutations/RemoveApartmentMutation';

const ApartmentsList = ({viewer, relay}) => {
    const { apartments } = viewer
    const remove = (id) => {
        RemoveApartmentMutation.commit(
            relay.environment,
            id,
            viewer
        )
    }
    return (
        <ul>
            {apartments.edges && apartments.edges.map(({node: {id, title, slug}}) =>
                <li key={id}><Link to={`/apartment/${slug}`}>{title}</Link> <button onClick={() => remove(id)}>X</button></li>
            )}
        </ul>
    )
}

export default createFragmentContainer(
    ApartmentsList,
    graphql`
        fragment ApartmentsList_viewer on User {
            id,
            apartments(
                first: 100
            ) @connection(key: "ApartmentsList__apartments") {
                edges {
                    node {
                        id,
                        title,
                        slug
                    }
                }   
            }
        }
    `
)

/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteFragment } from 'relay-runtime';
import type { FragmentReference } from 'relay-runtime';
declare export opaque type ApartmentItem_apartment$ref: FragmentReference;
export type ApartmentItem_apartment = {|
  +id: string,
  +title: ?string,
  +slug: ?string,
  +$refType: ApartmentItem_apartment$ref,
|};
*/


const node/*: ConcreteFragment*/ = {
  "kind": "Fragment",
  "name": "ApartmentItem_apartment",
  "type": "Apartment",
  "metadata": null,
  "argumentDefinitions": [],
  "selections": [
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "id",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "title",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "slug",
      "args": null,
      "storageKey": null
    }
  ]
};
(node/*: any*/).hash = '22e2b02d7183e787b0f4b8dc7e021f09';
module.exports = node;

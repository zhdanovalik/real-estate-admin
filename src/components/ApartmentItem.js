import React from 'react';
import { createFragmentContainer, graphql } from 'react-relay';

class ApartmentItem extends React.Component {
    super(props) {

    }

    render() {
        const { id, title, slug } = this.props.apartment
        return (
            <div>
                Item page - {title}
            </div>
        )
    }
}

export default createFragmentContainer(
    ApartmentItem,
    graphql`
        fragment ApartmentItem_apartment on Apartment {
            id,
            title,
            slug,
        }
    `
)
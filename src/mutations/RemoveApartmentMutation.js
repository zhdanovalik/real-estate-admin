import {
    commitMutation,
    graphql,
} from 'react-relay';
import { ConnectionHandler } from 'relay-runtime';

const mutation = graphql`
    mutation RemoveApartmentMutation($input: RemoveApartmentInput!) {
        removeApartment(input: $input) {
            removedId,
        }
    }
`;

function sharedUpdater(store, user, deletedID) {
    const userProxy = store.get(user.id);

    const conn = ConnectionHandler.getConnection(
        userProxy,
        'ApartmentsList__apartments',
    );

    ConnectionHandler.deleteNode(
        conn,
        deletedID,
    );
}

function commit(
    environment,
    id,
    user
) {
    return commitMutation(
        environment,
        {
            mutation,
            variables: {
                input: { id },
            },
            updater: (store) => {
                const payload = store.getRootField('removeApartment');
                sharedUpdater(store, user, payload.getValue('removedId'));
            },
            optimisticUpdater: (store) => {
                sharedUpdater(store, user, id);
            },
        }
    );
}

export default { commit };

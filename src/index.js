// import React from 'react';
// import ReactDOM from 'react-dom';
// import { QueryRenderer, graphql } from 'react-relay';
// import environment from './environment';
// import './index.css';
// import App from './App';
// import registerServiceWorker from './registerServiceWorker';
//
// ReactDOM.render(
//     <QueryRenderer
//         environment={environment}
//         query={graphql`
//           query srcQuery {
//             viewer {
//                 ...App_viewer
//             }
//           }
//         `}
//         variables={{}}
//         render={({error, props}) => {
//             if (error) {
//                 return <div>{error.message}</div>;
//             } else if (props) {
//                 return <App {...props}/>;
//             }
//             return <div>Loading</div>;
//         }}
//     />,
//     document.getElementById('root'));
// registerServiceWorker();

import HashProtocol from 'farce/lib/HashProtocol';
import BrowserProtocol from 'farce/lib/BrowserProtocol';
import queryMiddleware from 'farce/lib/queryMiddleware';
import createFarceRouter from 'found/lib/createFarceRouter';
import createRender from 'found/lib/createRender';
import { Resolver } from 'found-relay';
import React from 'react';
import ReactDOM from 'react-dom';
import environment from './environment'
import routes from './routes';

const Router = createFarceRouter({
    historyProtocol: new BrowserProtocol(),
    historyMiddlewares: [queryMiddleware],
    routeConfig: routes,

    render: createRender({}),
});

// const mountNode = document.createElement('div');
// document.body.appendChild(mountNode);

ReactDOM.render(<Router resolver={new Resolver(environment)} />, document.getElementById('root'));

